const express = require("express");
const querystring = require("querystring");
//sets app to express method
const app = express();


// empty array where all the messages will be pushed 
let messages = [];

// Track last active times for each sender
let users = {};

//middleware that will tell express to use the public folder and json parse function
app.use(express.static("./public"));
app.use(express.json());

//onto the mongoose stuff. the first line require mongoose module. the next line 
//uses connect function to connect to the mongo database through a url 
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/klack', { useNewUrlParser: true });

// this checks to see if the mongoose connection has an error or if it works
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
  // console.log(db)
  console.log(db)
});

//define schema
var messageSchema = new mongoose.Schema({
  sender: String,
  message: String,
  timestamp: Number
});

//create model from schema
let Message = mongoose.model('Response', messageSchema)

// generic comparison function for case-insensitive alphabetic sorting on the name field
function userSortFn(a, b) {
  var nameA = a.name.toUpperCase(); // ignore upper and lowercase
  var nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
}

//get request functionality.
app.get("/messages", (request, response) => {
  // get the current time
  const now = Date.now();

  // consider users active if they have connected (GET or POST) in last 15 seconds
  const requireActiveSince = now - 15 * 1000;

  // create a new list of users with a flag indicating whether they have been active recently
  usersSimple = Object.keys(users).map(x => ({
    name: x,
    active: users[x] > requireActiveSince
  }));

  // sort the list of users alphabetically by name
  usersSimple.sort(userSortFn);
  usersSimple.filter(a => a.name !== request.query.for);

  // update the requesting user's last access time
  users[request.query.for] = now;

  Message.find().sort({
    timestamp: 'asc'
}).exec(function (err, msgs) {
    msgs.forEach(msg => {
        messages.push(msg);
        if (!users[msg.sender]) {
            users[msg.sender] = msg.timestamp
        } else if (users[msg.sender] < msg.timestamp) {
            users[msg.sender] = msg.timestamp
        }
    })
  })

  // send the latest 40 messages and the full user list, annotated with active flags
  response.send({ messages: messages.slice(-40), users: usersSimple });
})

//this is the post method for posting new messages
app.post("/messages", (request, response) => {
  // add a timestamp to each incoming message.
  console.log(request.body)
  const timestamp = Date.now();
  request.body.timestamp = timestamp;

  // append the new message to the message list
  var message = new Message({
    sender: request.body.sender,
    message: request.body.message,
    timestamp: request.body.timestamp
});
// Save to database 
message.save()
    .then(data => {
        console.log('msg saved to the database:', data);
    })
    .catch(err => {
        console.log('Unable to save to database');
    });


    
  // update the posting user's last access timestamp (so we know they are active)
  users[request.body.sender] = timestamp;

  // Send back the successful response.
  response.status(201);
  response.send(request.body);
});

app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening... ");
});